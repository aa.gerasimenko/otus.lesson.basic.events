﻿internal class Program
{
    // {Thread.CurrentThread.ManagedThreadId}

    private static void Main(string[] args)
    {
        CustomerNotificationService customerNotificationService = new CustomerNotificationService();
        DeliveryNotificationService deliveryNotificationService = new DeliveryNotificationService();
        OrderService orderService = new OrderService();

        // subscriptions
        orderService.OrderStateChanged += customerNotificationService.OnCustomerOrderStateChanged;
        orderService.OrderPrepared += customerNotificationService.OnCustomerOrderPrepared;
        orderService.OrderPrepared += deliveryNotificationService.OnCustomerOrderPrepared;

        // order for customer
        RunOrdersAsync(orderService);

        Console.ReadLine();
    }

    private static async Task RunOrdersAsync(OrderService orderService)
    {
        Customer customer1 = new Customer("Otus");
        var order1 = orderService.CreateOrder(customer1.Id);
        var order2 = orderService.CreateOrder(customer1.Id);

        Customer customer2 = new Customer("Anton");
        var order3 = orderService.CreateOrder(customer2.Id);
        var order4 = orderService.CreateOrder(customer2.Id);

        var t1 = orderService.PrepareOrderAsync(order1);
        var t2 = orderService.PrepareOrderAsync(order2);
        var t3 = orderService.PrepareOrderAsync(order3);
        var t4 = orderService.PrepareOrderAsync(order4);

        await Task.WhenAll(t1, t2, t3, t4);
    }

    public class Customer
    {
        public Customer(string id)
        {
            Id = id;
        }

        public string Id { get; set; }
    }

    public class CustomerNotificationService
    {
        public void OnCustomerOrderStateChanged(Order order)
        {
            Console.WriteLine($"{Environment.CurrentManagedThreadId}. new state of order {order.OrderId}, state _{order.State}_ for **{order.CustomerId}**");
        }
        public void OnCustomerOrderPrepared(Order order)
        {
            Console.WriteLine($"{Environment.CurrentManagedThreadId}. Order {order.OrderId} for **{order.CustomerId}** prepared");
        }
    }

    public class DeliveryNotificationService
    {
        public void OnCustomerOrderPrepared(Order order)
        {
            Console.WriteLine($"{Environment.CurrentManagedThreadId}. Take order {order.OrderId} for **{order.CustomerId}**");
        }
    }

    public class Order
    {
        static int _counter = 0;
        public Order(string customerId)
        {
            CustomerId = customerId;
            State = "new";
            OrderId = $"order_{_counter++}";
        }

        public string CustomerId { get; set; }
        public string OrderId { get; set; }
        public string State { get; private set; }

        public void SetState(string newState)
        {
            State = newState;
        }
    }

    public class OrderService
    {
        public Order CreateOrder(string customerId)
        {
            return new Order(customerId);
        }

        public event Action<Order> OrderStateChanged;
        public event Action<Order> OrderPrepared;

        public async Task PrepareOrderAsync(Order order)
        {
            order.SetState("start");
            OrderStateChanged.Invoke(order);
            await Task.Delay(1000);

            order.SetState("45%");
            OrderStateChanged.Invoke(order);
            await Task.Delay(1000);

            order.SetState("90%");
            OrderStateChanged.Invoke(order);
            await Task.Delay(1000);

            order.SetState("ready");
            OrderStateChanged.Invoke(order);
            OrderPrepared.Invoke(order);
        }
    }
}